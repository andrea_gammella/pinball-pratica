// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PINBALL_PinballGameModeBase_generated_h
#error "PinballGameModeBase.generated.h already included, missing '#pragma once' in PinballGameModeBase.h"
#endif
#define PINBALL_PinballGameModeBase_generated_h

#define Pinball_Source_Pinball_PinballGameModeBase_h_15_SPARSE_DATA
#define Pinball_Source_Pinball_PinballGameModeBase_h_15_RPC_WRAPPERS
#define Pinball_Source_Pinball_PinballGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Pinball_Source_Pinball_PinballGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPinballGameModeBase(); \
	friend struct Z_Construct_UClass_APinballGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APinballGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pinball"), NO_API) \
	DECLARE_SERIALIZER(APinballGameModeBase)


#define Pinball_Source_Pinball_PinballGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPinballGameModeBase(); \
	friend struct Z_Construct_UClass_APinballGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APinballGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pinball"), NO_API) \
	DECLARE_SERIALIZER(APinballGameModeBase)


#define Pinball_Source_Pinball_PinballGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APinballGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APinballGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinballGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinballGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinballGameModeBase(APinballGameModeBase&&); \
	NO_API APinballGameModeBase(const APinballGameModeBase&); \
public:


#define Pinball_Source_Pinball_PinballGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APinballGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinballGameModeBase(APinballGameModeBase&&); \
	NO_API APinballGameModeBase(const APinballGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinballGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinballGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APinballGameModeBase)


#define Pinball_Source_Pinball_PinballGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Pinball_Source_Pinball_PinballGameModeBase_h_12_PROLOG
#define Pinball_Source_Pinball_PinballGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_SPARSE_DATA \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_RPC_WRAPPERS \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_INCLASS \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pinball_Source_Pinball_PinballGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_SPARSE_DATA \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Pinball_Source_Pinball_PinballGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PINBALL_API UClass* StaticClass<class APinballGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pinball_Source_Pinball_PinballGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
