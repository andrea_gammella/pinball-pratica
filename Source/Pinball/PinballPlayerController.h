// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PinballPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PINBALL_API APinballPlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
