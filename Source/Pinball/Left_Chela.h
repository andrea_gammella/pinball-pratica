// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Left_Chela.generated.h"

UCLASS()
class PINBALL_API ALeft_Chela : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ALeft_Chela();

	FRotator CurrentRotation;
	FRotator InitialRotatation;
	float speed;

	bool Movement = false;

	void Move_Left_Arm();

	void Stop_Movement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
