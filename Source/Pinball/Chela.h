// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Chela.generated.h"

UCLASS()
class PINBALL_API AChela : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AChela();

	FRotator CurrentRotation;
	FRotator InitialRotatation;
	float speed;

	bool Movement = false;

	void Move_Right_Arm();

	void Stop_Movement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
