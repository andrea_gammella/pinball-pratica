// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/InputComponent.h"
#include "Chela.h"

// Sets default values
AChela::AChela()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	
}

// Called when the game starts or when spawned
void AChela::BeginPlay()
{
	Super::BeginPlay();

	CurrentRotation = this->GetActorRotation();
	InitialRotatation = CurrentRotation;
	speed = 300.f;
	
}

// Called every frame
void AChela::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
	
	if (Movement)
	{
		if (CurrentRotation.Yaw >= -60.f)
		{
			SetActorRotation(InitialRotatation);
		}
		else
		{
			CurrentRotation.Yaw += speed * DeltaTime;
			SetActorRotation(CurrentRotation);
		}
	}
	else
	{
		SetActorRotation(InitialRotatation);
	}
}

// Called to bind functionality to input
void AChela::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("Move_R_Arm", IE_Pressed, this, &AChela::Move_Right_Arm);
	InputComponent->BindAction("Move_R_Arm", IE_Released, this, &AChela::Stop_Movement);
}

void AChela::Move_Right_Arm()
{
	Movement = true;
}

void AChela::Stop_Movement()
{
	Movement = false;

	CurrentRotation = InitialRotatation;
}