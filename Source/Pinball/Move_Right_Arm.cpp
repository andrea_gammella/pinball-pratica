// Fill out your copyright notice in the Description page of Project Settings.


#include "GameFramework/PlayerController.h"
#include "Components/InputComponent.h"
#include "Move_Right_Arm.h"

// Sets default values for this component's properties
UMove_Right_Arm::UMove_Right_Arm()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
}


// Called when the game starts
void UMove_Right_Arm::BeginPlay()
{
	Super::BeginPlay();

	InitialRotation = GetOwner()->GetActorRotation();
	HitRotation = InitialRotation + FRotator(0.f, 0.f, 40.f);
	
}


// Called every frame
void UMove_Right_Arm::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

