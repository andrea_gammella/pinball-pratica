// Fill out your copyright notice in the Description page of Project Settings.


#include "Left_Chela.h"

// Sets default values
ALeft_Chela::ALeft_Chela()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void ALeft_Chela::BeginPlay()
{
	Super::BeginPlay();

	CurrentRotation = this->GetActorRotation();
	InitialRotatation = CurrentRotation;
	speed = 300.f;
	
}

// Called every frame
void ALeft_Chela::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Movement)
	{
		if (CurrentRotation.Yaw <= 60.f)
		{
			SetActorRotation(InitialRotatation);
		}
		else
		{
			CurrentRotation.Yaw += speed * DeltaTime;
			SetActorRotation(CurrentRotation);
		}
	}
	else
	{
		SetActorRotation(InitialRotatation);
	}

}

// Called to bind functionality to input
void ALeft_Chela::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("Move_L_Arm", IE_Pressed, this, &ALeft_Chela::Move_Left_Arm);
	InputComponent->BindAction("Move_L_Arm", IE_Released, this, &ALeft_Chela::Stop_Movement);
}

void ALeft_Chela::Move_Left_Arm()
{
	Movement = true;
}

void ALeft_Chela::Stop_Movement()
{
	Movement = false;

	CurrentRotation = InitialRotatation;
}

